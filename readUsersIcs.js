const dotenv = require('dotenv');
const request = require('request-promise');
const schedule = require('node-schedule');
const amqplib = require('amqplib');

dotenv.config();

const AMQP_SERVER = (() => {
  if (!process.env.AMQP_SERVER) {
    throw new Error('Need AMQP_SERVER env var');
  }

  return process.env.AMQP_SERVER;
})();

const AMQP_EXCHANGE = (() => {
  if (!process.env.AMQP_EXCHANGE) {
    throw new Error('Need AMQP_EXCHANGE env var');
  }

  return process.env.AMQP_EXCHANGE;
})();

const AMQP_PATTERN = (() => {
  if (!process.env.AMQP_PATTERN) {
    throw new Error('Need AMQP_PATTERN env var');
  }

  return process.env.AMQP_PATTERN;
})();

const USERS_API_URL = (() => {
  if (!process.env.USERS_API_URL) {
    throw new Error('Need USERS_API_URL env var');
  }

  return process.env.USERS_API_URL;
})();

const RUN_EVERY_X_MIN = process.env.RUN_EVERY_X_MIN || 60;

console.log('CRON schedule every x mins :', RUN_EVERY_X_MIN);

const ICS_URL = `${USERS_API_URL}/users-ics/`;

const createAMQPClient = async (server, exchange, topic) => {
  const connection = await amqplib.connect(`amqp://${server}`);
  const channel    = await connection.createChannel();

  await channel.assertExchange(
    exchange,
    'topic',
    { durable: true }
  );

  return {
    send: (userId, url) => {
      const content = JSON.stringify({
        _v: 0,
        userId,
        url,
      });

      console.log('send', `${topic}.${userId}`,  content);

      return channel.publish(
        exchange,
        `${topic}.${userId}`,
        Buffer.from(content)
      );
    }
  };
};

const getIcs = async (url) => request
  .get(url, { json: true })
  .then((body) => body.ics);

const init = async () => {
  try {
    const amqpClient = await createAMQPClient(
      AMQP_SERVER,
      AMQP_EXCHANGE,
      AMQP_PATTERN
    );

    schedule.scheduleJob(`0 */${RUN_EVERY_X_MIN} * * * *`, async () => {
      console.log('Get all ics');
      const ics = await getIcs(ICS_URL);
      console.log('ics', ics);

      ics.forEach(({ user_id, url }) => amqpClient.send(user_id, url));
    });
  } catch(e) {
    throw e;
  }
};

init();

