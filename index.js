const dotenv = require('dotenv');
const { getFile, getEvents } = require('./src/readIcs');
const { createAMQPClient } = require('./src/amqp');

dotenv.config();

const AMQP_SERVER = (() => {
  if (!process.env.AMQP_SERVER) {
    throw new Error('Need AMQP_SERVER env var');
  }

  return process.env.AMQP_SERVER;
})();

const AMQP_EXCHANGE = (() => {
  if (!process.env.AMQP_EXCHANGE) {
    throw new Error('Need AMQP_EXCHANGE env var');
  }

  return process.env.AMQP_EXCHANGE;
})();

const AMQP_PATTERN = (() => {
  if (!process.env.AMQP_PATTERN) {
    throw new Error('Need AMQP_PATTERN env var');
  }

  return process.env.AMQP_PATTERN;
})();

const EVENTS_API_URL = (() => {
  if (!process.env.EVENTS_API_URL) {
    throw new Error('Need EVENTS_API_URL env var');
  }

  return process.env.EVENTS_API_URL;
})();

const { postEvent } = require('./src/eventsApi')({
  apiUrl: EVENTS_API_URL,
});

const doTask = async (task) => {
  if (task._v <= 0) {
    console.log('New task', task.url);
    try {
      const icsContent = await getFile(task.url);
      const events = getEvents(icsContent);

      console.log('events', events.map((e) => e.summary));

      events.forEach((event) => postEvent({
        id:         `${event.uid}+${task.userId}`,
        name:        event.summary,
        description: event.description,
        source:      task.url,
        userId:      task.userId,
        startDate:   event.startDate,
        endDate:     event.endDate,
      }));
    } catch(e) {
      console.error(`Can’t read ${task.url}`);
    }
  }
};

const worker = async () => {
  try {
    const amqpClient = await createAMQPClient(
      AMQP_SERVER,
      AMQP_EXCHANGE,
      `${AMQP_PATTERN}.*`
    );

    amqpClient.onMessage((msg) => {
      const task = JSON.parse(msg.content);
      doTask(task);
    });

    console.log('AMQP client listening on', AMQP_EXCHANGE, 'for', AMQP_PATTERN);
  } catch(e) {
    throw e;
  }
};

worker();
