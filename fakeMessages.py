import pika
import json

connection = pika.BlockingConnection(pika.ConnectionParameters('localhost'))
channel = connection.channel()

channel.exchange_declare(
    exchange = 'events',
    exchange_type = 'topic',
    durable = True
)

message = {
    '_v': 0,
    'url': 'http://ics-proxy.d.wuips.com/?user=e132397K&password=clgkwibuscuranokitel&url=edt.univ-nantes.fr/chantrerie-gavy/g1609.ics',
    'userId': '1'
}

channel.basic_publish(
    exchange = 'events',
    routing_key = 'task.events.read_ics',
    body = json.dumps(message),
    properties=pika.BasicProperties(
        content_type = 'application/json',
        delivery_mode = 2
    )
)
