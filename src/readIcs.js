const http = require('http');
const IcalParser = require('ical-expander');
const request = require('request-promise');
const moment = require('moment');

const inPeriod = (periodStart, periodEnd) => (start, end) => (
  start.isBetween(periodStart, periodEnd)
  || end.isBetween(periodStart, periodEnd)
  || (start.isBefore(periodStart) && end.isAfter(periodEnd))
);

const getFile = (fileUrl) => request.get(fileUrl);

const getEvents = (icsContent) => {
  const ical = new IcalParser({
    ics: icsContent,
    maxIterations: 100,
  });

  const events = ical.all();

  const inNext2Hours = inPeriod(
    moment(),
    moment().add(2, 'hour').subtract(2, 'seconds')
  );

  return currentEvents = [
    ...events.events,
    ...events.occurrences,
  ].map((e) => {
      e.start = moment(e.startDate.toJSDate());
      e.end   = moment(e.endDate.toJSDate());

      return e;
    })
    .filter((e) => inNext2Hours(e.start, e.end))
    .map((e) => ({
      startDate:  e.start.toDate(),
      endDate:    e.end.toDate(),
      duration:   e.duration.toSeconds(),
      summary:    e.summary,
      descripton: e.description,
      attendes:   e.attendees,
      organizer:  e.organizer,
      uid:        e.uid,
    }));
};

module.exports = {
  getFile,
  getEvents,
};
