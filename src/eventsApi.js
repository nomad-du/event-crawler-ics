const request = require('request-promise');

const createApi = ({ apiUrl }) => ({
  postEvent: (event) => request
    .post(`${apiUrl}/events/`, { body: { event }, json: true, })
  .catch((e) => {
    console.error(`Failed to post event from ${event.source}`)
  }),
});

module.exports = createApi;
